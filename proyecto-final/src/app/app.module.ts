import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DragulaModule } from 'ng2-dragula';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ProyectoService } from './proyecto.service';
import { PersonaService } from './persona.service';
import { EstadoTareaService } from './estado-tarea.service';

import { TareaService } from './tarea.service';

import { AppComponent } from './app.component';
import { PersonaComponent } from './persona/persona.component';
import { EstadoTareasComponent } from './estado-tareas/estado-tareas.component';
import { ProyectoComponent } from './proyecto/proyecto.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TareaComponent } from './tarea/tarea.component';
import { ReporteEstadoComponent } from './reporte-estado/reporte-estado.component';
import { ReportePersonaComponent } from './reporte-persona/reporte-persona.component';
import { ReporteProyectoComponent } from './reporte-proyecto/reporte-proyecto.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonaComponent,
    EstadoTareasComponent,
    ProyectoComponent,
    DashboardComponent,
    TareaComponent,
    ReporteEstadoComponent,
    ReportePersonaComponent,
    ReporteProyectoComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    DragulaModule
  ],
  providers: [
  ProyectoService,
  PersonaService,
  EstadoTareaService,
  TareaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


