import { Component, OnInit } from '@angular/core';
import { Proyecto } from '../proyecto';
import { ProyectoService } from '../proyecto.service';
import { Tarea } from '../tarea';
import { TareaService } from '../tarea.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reporte-proyecto',
  templateUrl: './reporte-proyecto.component.html',
  styleUrls: ['./reporte-proyecto.component.css']
})
export class ReporteProyectoComponent implements OnInit {
  idProyecto: Proyecto;
  proyecto: Proyecto[];
  tareas: Tarea[];
  private sub: any;
  id: string;
  constructor(private route: ActivatedRoute, 
   			   private service: ProyectoService,
           private service_tarea: TareaService) { }

  ngOnInit() {
  	this.sub = this.route.params.subscribe(params => {this.idProyecto = this.service.findById(params['id']);});
     this.proyecto= this.service.read();
    this.tareas = this.service_tarea.readForIdProyecto(this.idProyecto.id);
  }

}
