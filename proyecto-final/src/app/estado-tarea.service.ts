import { Injectable } from '@angular/core';
import { Estado_tarea } from './estado-tarea';

@Injectable()
export class EstadoTareaService {
  data: Estado_tarea[];
  constructor() { 
  	this.data = JSON.parse(localStorage.getItem('estado-tareas') || '[]');

  }

  read() {
	this.data = JSON.parse(localStorage.getItem('estado-tareas') || '[]');
	return this.data;
  }

  save(data: Estado_tarea[]) {
    this.data = data;
    localStorage.setItem('estado-tareas', JSON.stringify(this.data));
  }
   findById(id: number) {
    return this.data.find(x => x.id == id);
  }

  findByDescripcion(descripcion: string){
    return this.data.find(x => x.descripcion == descripcion)
      
  }
  

}


