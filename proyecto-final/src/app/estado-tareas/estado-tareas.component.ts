import { Component, OnInit } from '@angular/core';
import { Estado_tarea} from '../estado-tarea';
import { EstadoTareaService} from '../estado-tarea.service'

@Component({
  selector: 'app-estado-tareas',
  templateUrl: './estado-tareas.component.html',
  styleUrls: ['./estado-tareas.component.css']
})
export class EstadoTareasComponent implements OnInit {
  
  data: Estado_tarea[];
  current_estado_tarea: Estado_tarea;
  crud_operation = { is_new: false, is_visible: false };
  constructor(private service: EstadoTareaService) { 
  }

  ngOnInit() {
  	this.data = this.service.read();
  	this.current_estado_tarea = new Estado_tarea();
  }

  new() {
  	this.current_estado_tarea = new Estado_tarea();
  	this.crud_operation.is_visible = true;
  	this.crud_operation.is_new = true;
  }

  edit(row) {
  	this.crud_operation.is_visible = true;
  	this.crud_operation.is_new = false;
  	this.current_estado_tarea = row;
  }

  delete(row) {
  	this.crud_operation.is_new = false;
  	const index = this.data.indexOf(row,0);
  	if(index > -1) {
  		this.data.splice(index, 1);
  	}
  	this.save();
  }

  save() {
  	if(this.crud_operation.is_new) {
  		this.data.push(this.current_estado_tarea);
  	}
  	this.service.save(this.data);
    localStorage.setItem('estado-tareas', JSON.stringify(this.data));
  	this.current_estado_tarea = new Estado_tarea();
  	this.crud_operation.is_visible = false;
  }
}
