import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoTareasComponent } from './estado-tareas.component';

describe('EstadoTareasComponent', () => {
  let component: EstadoTareasComponent;
  let fixture: ComponentFixture<EstadoTareasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoTareasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
