import { Component, OnInit } from '@angular/core';
import { Persona } from '../persona';
import { PersonaService } from '../persona.service';
import { Tarea } from '../tarea';
import { TareaService } from '../tarea.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-reporte-persona',
  templateUrl: './reporte-persona.component.html',
  styleUrls: ['./reporte-persona.component.css']
})
export class ReportePersonaComponent implements OnInit {
  persona: Persona;
  person: Persona[];
  tareas: Tarea[];
  private sub: any;
  id: string;
  constructor(private route: ActivatedRoute, 
   			   private service: PersonaService,
           private service_tarea: TareaService) { }


 ngOnInit() {
     	this.sub = this.route.params.subscribe(params => {this.persona = this.service.findById(params['id']);});
      this.person= this.service.read();
      this.tareas = this.service_tarea.readForIdPersona(this.persona.id);

  }
  

}
