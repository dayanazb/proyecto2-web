
import { Component, OnInit } from '@angular/core';
import { Tarea } from '../tarea';
import { TareaService } from '../tarea.service';
import { Proyecto } from '../proyecto';
import { ProyectoService } from '../proyecto.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.css']
})
export class TareaComponent implements OnInit {
  proyecto: Proyecto;
  data: Tarea[];
  current_tarea: Tarea;
  crud_operation = { is_new: false, is_visible: false };
  private sub: any;
  id: string;
  constructor(private service: TareaService, private proyectoService: ProyectoService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {this.proyecto = this.proyectoService.findById(params['id']);});
  	this.data = this.service.read();
    this.current_tarea = new Tarea();
  }

  new() { 
    this.current_tarea = new Tarea();
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }

  edit(row) {
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = false;
    this.current_tarea = row;
  }


  delete(row) {
    this.crud_operation.is_new = false;
    const index = this.data.indexOf(row, 0);
    if (index > -1) {
      this.data.splice(index, 1);
    }
    this.save();
  }

  save() {
    if (this.crud_operation.is_new) {
      this.data.push(this.current_tarea);
    }
    this.service.save(this.data);
    this.current_tarea = new Tarea();
    this.crud_operation.is_visible = false;
  }

}
