
import { Component, OnInit } from '@angular/core';
import { Proyecto } from '../proyecto';
import { ProyectoService } from '../proyecto.service';
import { Estado_tarea } from '../estado-tarea';
import { EstadoTareaService } from '../estado-tarea.service';
import { Tarea } from '../tarea';
import { TareaService } from '../tarea.service';
import { Persona } from '../persona';
import { PersonaService } from '../persona.service';
import { ActivatedRoute } from '@angular/router';




@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})

export class DashboardComponent implements OnInit {
  persona: Persona
  proyecto: Proyecto;
  estado_tareas: Estado_tarea[];
  personas: Persona[];
  tareas: Tarea[];
  private sub: any;
  id: string;

   constructor(private route: ActivatedRoute, 
   			   private service: ProyectoService,
   			   private service_estado_tareas: EstadoTareaService,
           private service_tarea: TareaService, 
           private service_persona: PersonaService ) { }

  ngOnInit() {
  	this.sub = this.route.params.subscribe(params => {this.proyecto = this.service.findById(params['id']);});
    this.estado_tareas = this.service_estado_tareas.read();
    this.personas = this.service_persona.read();
    this.tareas = this.service_tarea.read();
    this.tareas = this.service_tarea.readForId(this.proyecto.id);
  }
}
