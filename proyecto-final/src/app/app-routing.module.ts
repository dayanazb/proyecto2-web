import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PersonaComponent } from './persona/persona.component';
import { ProyectoComponent } from './proyecto/proyecto.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EstadoTareasComponent } from './estado-tareas/estado-tareas.component';
import { TareaComponent } from './tarea/tarea.component'
import { ReporteEstadoComponent } from './reporte-estado/reporte-estado.component';
import { ReportePersonaComponent } from './reporte-persona/reporte-persona.component';
import { ReporteProyectoComponent } from './reporte-proyecto/reporte-proyecto.component';

const routes: Routes = [
  { path: 'personas', component: PersonaComponent },
  { path: 'proyectos', component: ProyectoComponent},
  { path: 'proyectos/:id/dashboard', component: DashboardComponent},
  { path: 'estado-tareas', component: EstadoTareasComponent},
  { path: 'tareas', component: TareaComponent},
  { path: 'estados/:id/reporte-estado', component: ReporteEstadoComponent},
  { path: 'personas/:id/reporte-persona', component: ReportePersonaComponent},
  { path: 'proyectos/:id/reporte-proyecto', component: ReporteProyectoComponent},
  { path: '', component:  ProyectoComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
