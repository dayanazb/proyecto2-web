import { Component, OnInit } from '@angular/core';
import { Estado_tarea } from '../estado-tarea';
import { EstadoTareaService } from '../estado-tarea.service';
import { Tarea } from '../tarea';
import { TareaService } from '../tarea.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reporte-estado',
  templateUrl: './reporte-estado.component.html',
  styleUrls: ['./reporte-estado.component.css']
})
export class ReporteEstadoComponent implements OnInit {
  estado: Estado_tarea;
  estadoTarea: Estado_tarea[];
  tareas: Tarea[];
  private sub: any;
  id: string;
  constructor(private route: ActivatedRoute, 
   			   private service: EstadoTareaService,
           private service_tarea: TareaService) { }

  ngOnInit() {
  	this.sub = this.route.params.subscribe(params => {this.estado = this.service.findById(params['id']);});
     this.estadoTarea= this.service.read();
    this.tareas = this.service_tarea.readForIdEstado(this.estado.id);
  }

}
