import { TestBed, inject } from '@angular/core/testing';

import { EstadoTareaService } from './estado-tarea.service';

describe('EstadoTareaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstadoTareaService]
    });
  });

  it('should be created', inject([EstadoTareaService], (service: EstadoTareaService) => {
    expect(service).toBeTruthy();
  }));
});
