
import { Injectable } from '@angular/core';
import { Tarea } from './tarea';

@Injectable()
export class TareaService {
  data: Tarea[];

  constructor() { 
  	 this.data = JSON.parse(localStorage.getItem('tareas') || '[]');
  }

   read() {
    this.data = JSON.parse(localStorage.getItem('tareas') || '[]');
    return this.data;
  }

  readForId(id: number){
    var retdata = [];
    for (var i = 0; i < this.data.length; i++) {

      if (this.data[i].idProyecto == id) {
        retdata.push(this.data[i]);        
      }
    }
    return retdata;
  }
  readForIdEstado(id: number){
    var returnda = [];
    for (var i = 0; i < this.data.length; i++) {

      if (this.data[i].idEstadoTareas == id) {
        returnda.push(this.data[i]);        
      }
    }
    return returnda;
  }

readForIdPersona(id: number){
    var returnda = [];
    for (var i = 0; i < this.data.length; i++) {

      if (this.data[i].idPersona == id) {
        returnda.push(this.data[i]);        
      }
    }
    return returnda;
  }
  readForIdProyecto(id: number){
    var returnda = [];
    for (var i = 0; i < this.data.length; i++) {

      if (this.data[i].idProyecto == id) {
        returnda.push(this.data[i]);        
      }
    }
    return returnda;
  }
  save(data: Tarea[]) {
    this.data = data;
    localStorage.setItem('tareas', JSON.stringify(this.data));
  }

  findById(id: number) {
    return this.data.find(x => x.id == id);
  }

  findByDescripcion(descripcion: string){
    return this.data.find(x => x.descripcion == descripcion)
      
  }

  findByPerson(person: number){
    return this.data.find(x => x.idPersona == person)
      
  }

  findByIdProyect(idProyecto: number){
    return this.data.find(x => x.idProyecto == idProyecto)
      
  }

}
